export const INITIAL_LOCATION_STATE = { lat: 38.736946, lng: -9.142685 };

export default class Truck {
  constructor(currentLocation = INITIAL_LOCATION_STATE, path, firstLocation) {
    this.currentLocation = currentLocation;
    this.path = path;
    this.firstLocation = firstLocation;
  }
}
