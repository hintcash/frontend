export class PoiType {
    constructor(name, icon, searchType) {
        this.name = name;
        this.icon = icon;
        this.searchType = searchType;
    }
}

export const POI_TYPE = {
    ALL: new PoiType('View All', '', 'all'),
    GAS_STATIONS: new PoiType('Gas Station', 'icn-gas-station.png', 'gas_station'),
    RESTAURANTS: new PoiType('Restaurants', 'icn-restaurant.png', 'restaurant'),
    HOTELS: new PoiType('Hotels', 'icn-hotel.png', 'lodging')
}

const POI_TYPES = [POI_TYPE.ALL, POI_TYPE.GAS_STATIONS, POI_TYPE.RESTAURANTS, POI_TYPE.HOTELS];

const POI_SEARCH_TYPES = [POI_TYPE.GAS_STATIONS, POI_TYPE.RESTAURANTS, POI_TYPE.HOTELS];

class POI {
    constructor() {
        this.TYPE = POI_TYPE;
        this.TYPES = POI_TYPES;
    }

    getAsListBySearchType(searchType) {
        return searchType === this.TYPE.ALL.searchType
            ? POI_SEARCH_TYPES
            : this.TYPES.filter(element => element.searchType === searchType);
    }
}

export default new POI();