import React, { Component } from 'react';
import './Map.css';
import MapService from '../../services/MapService';

class Map extends Component {
  constructor(props) {
    super(props);
    this.onScriptLoad = this.onScriptLoad.bind(this)
  }

  onScriptLoad() {
    let map = new window.google.maps.Map(document.getElementById(this.props.id), this.props.options);
    this.props.onMapLoad(new MapService(map, new window.google.maps.places.PlacesService(map)));
  }

  componentDidMount() {
    const url = `${process.env.REACT_APP_GOOGLE_MAPS_API_URL}?key=${process.env.REACT_APP_GOOGLE_MAPS_API_KEY}&libraries=places`;

    if (!window.google) {
      var scriptElement = document.createElement('script');
      scriptElement.type = 'text/javascript';
      scriptElement.src = url;
      var firstScriptElement = document.getElementsByTagName('script')[0];
      firstScriptElement.parentNode.insertBefore(scriptElement, firstScriptElement);
      scriptElement.addEventListener('load', e => {
        this.onScriptLoad()
      })
    } else {
      this.onScriptLoad()
    }
  }

  render() {
    return (
      <div className="full-width" id={this.props.id} />
    );
  }
}

export default Map