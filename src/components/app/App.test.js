import React from 'react';
import { mount } from 'enzyme';
import App from './App';
import Filter from '../filter/Filter';
import Map from '../map/Map';


test('experct true', () => {
  Map.prototype.componentDidMount = () => { };
  const wrapper = mount(<App />);

  expect(wrapper.find(Filter).exists()).toBe(true);
  expect(wrapper.find(Map).exists()).toBe(true);
});
