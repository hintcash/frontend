import React, { Component } from 'react';
import './App.css';
import Filter from '../filter/Filter';
import Map from '../map/Map';
import { INITIAL_LOCATION_STATE } from '../../model/Truck';
import TruckService from '../../services/TruckService';

class App extends Component {
  constructor() {
    super();
    this.truckService = new TruckService();
  }

  onMapLoad(mapService) {
    this.mapService = mapService;
  }

  mapOption() {
    return {
      center: INITIAL_LOCATION_STATE,
      zoom: 15,
      disableDefaultUI: true
    }
  }

  mapSearch(filter) {
    this.truckService.findByPlate(filter.licensePlate)
      .then(truck => {
        this.mapService.removeLine();
        this.mapService.removeAllMarkers();
        this.mapService.addTruckMarkers(truck);
        filter.poiTypes.forEach(type => this.mapService.findPlaces(truck.currentLocation, type, filter.radius))
      })
      .catch(error => console.log(error.response));
  }

  render() {
    return (
      <div>
        <Filter onFilterApply={filter => this.mapSearch(filter)} />
        <Map id="map" options={this.mapOption()} onMapLoad={mapService => { this.onMapLoad(mapService) }} />
      </div>
    );
  }
}

export default App;