import React, { Component } from 'react';
import './Filter.css';
import POI from '../../model/PoiType'

class Filter extends Component {
  constructor() {
    super();
    this.state = { filter: { licensePlate: '', poiType: '', radius: '' }, isOpen: false };
    this.poiTypes = POI.TYPES;
  }

  onChangeValue = ({ target }) => {
    const { name, value } = target;
    const { filter } = this.state;
    filter[name] = value;
    this.setState({ filter: filter });
  }

  applyEvent = () => {
    let { filter } = this.state;
    filter.poiTypes = POI.getAsListBySearchType(filter.poiType);
    this.props.onFilterApply(this.state.filter);
  }

  openMenu = () => {
    this.setState({ isOpen: !this.state.isOpen });
  }

  render() {
    const radius = [100, 200, 300, 500, 1000, 2000]
    return (
      <div className="box" >
        <div className="mobile-filter__menu-button">
          <button className="menu" onClick={this.openMenu}>
            <img className="mobile-filter__menu-button-icon" src="menu.png" alt="Menu button"></img>
          </button>
        </div>
        <div className={`filter ${this.state.isOpen ? '' : ' mobile-filter__menu-closed'}`}>
          <div className="filter-field">
            <input name="licensePlate" className="filter-field__input" placeholder="Search by licence plate" value={this.state.filter.licensePlate} onChange={this.onChangeValue} />
          </div>
          <div className="filter-field">
            <select name="poiType" className="filter-field__select" value={this.state.filter.poiType} onChange={this.onChangeValue}>
              <option value="" >Select POI type</option>
              {this.poiTypes.map((item) => <option key={item.searchType} value={item.searchType} >{item.name}</option>)}
            </select>
          </div>
          <div className="filter-field">
            <select name="radius" className="filter-field__select" value={this.state.filter.radius} onChange={this.onChangeValue}>
              <option>Select radius</option>
              {radius.map((item) => <option key={item} value={item} >{item}</option>)}
            </select>
          </div>
          <div className="filter-field">
            <button className="filter-field__button" onClick={this.applyEvent}>Apply</button>
          </div>
        </div>
      </div>
    );
  }
}

export default Filter;