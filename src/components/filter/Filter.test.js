import React from 'react';
import Filter from './Filter';
import { POI_TYPE } from '../../model/PoiType';
import { shallow } from 'enzyme';

test('Test filter', () => {
  const baseProps = {
    onFilterApply: jest.fn(),
  };

  const wrapper = shallow(<Filter {...baseProps} />);

  wrapper.setState({ filter: { licensePlate: '00-aa-00', poiType: 'gas_station', radius: 100 } });
  const expected = { licensePlate: '00-aa-00', poiType: 'gas_station', poiTypes: [POI_TYPE.GAS_STATIONS], radius: 100 };

  wrapper.instance().applyEvent();

  expect(baseProps.onFilterApply).toHaveBeenCalledTimes(1);
  expect(baseProps.onFilterApply).toHaveBeenCalledWith(expected);

});

test('Test expand', () => {
  const wrapper = shallow(<Filter />);

  wrapper.setState({ isOpen: false });
  wrapper.instance().openMenu();

  expect(wrapper.instance().state.isOpen).toBe(true);
});
