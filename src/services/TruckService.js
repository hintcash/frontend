import axios from 'axios';

export default class TruckService {
  constructor() {
    this.url = `${process.env.REACT_APP_API_URL}/truck`;
  }
  findByPlate(plate) {
    return axios.get(`${this.url}/${plate}`).then(response => response.data);
  }
}