const DEFAULT_RADIUS_VALUE = 500;

export default class MapService {
  constructor(map, placesService) {
    this.map = map;
    this.placesService = placesService;
    this.markers = [];
    this.pathLines = [];
  }

  getRequest(location, type, radius) {
    return {
      location: location,
      radius: radius || 500,
      type: type
    };
  }

  findPlaces(location, type, radius) {
      this.placesService.nearbySearch(this.getRequest(location, type.searchType, radius || DEFAULT_RADIUS_VALUE), (results, status) => {
        if (status === window.google.maps.places.PlacesServiceStatus.OK) {
          results.forEach(result => {
            result.distance = this.calculateDistance(location, { lat: result.geometry.location.lat(), lng: result.geometry.location.lng() });
            result.marker = this.createMarker(this.buildIcon(type.icon || result.icon), result.geometry.location);
            this.buildInfoWindow(result)
          });
          results.sort((previous, next) => previous.distance - next.distance);
          let closestPlace = results[0];
          closestPlace.infowindow.open(this.map, closestPlace.marker)
          this.drawLine(location, { lat: closestPlace.geometry.location.lat(), lng: closestPlace.geometry.location.lng() });
        }
    })
  }

  buildInfoWindow(result) {
    let contentString = `
      <div>Name: ${result.name}</div>
      <div>Distance: ${result.distance} km</div>
    `
    let infowindow = new window.google.maps.InfoWindow({
      content: contentString
    });
    result.infowindow = infowindow;

    result.marker.addListener('click', function() {
      infowindow.open(this.map, result.marker);
    });
  }

  calculateDistance(location1, location2) {
    let R = 6371.0710; // Radius of the Earth in kilometers
    let rlat1 = location1.lat * (Math.PI / 180); // Convert degrees to radians
    let rlat2 = location2.lat * (Math.PI / 180); // Convert degrees to radians
    let difflat = rlat2 - rlat1; // Radian difference (latitudes)
    let difflon = (location2.lng - location1.lng) * (Math.PI / 180); // Radian difference (longitudes)
    let distance = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat / 2) * Math.sin(difflat / 2) + Math.cos(rlat1) * Math.cos(rlat2) * Math.sin(difflon / 2) * Math.sin(difflon / 2)));
    return parseFloat(distance.toFixed(2));
  }

  createMarker(icon, location) {
    let marker = new window.google.maps.Marker({
      map: this.map,
      icon: icon,
      position: location
    });
    this.markers.push(marker);
    return marker;
  }

  buildIcon(icon, scaledSize = { w: 25, h: 25 }) {
    return {
      url: icon,
      scaledSize: new window.google.maps.Size(scaledSize.w, scaledSize.h)
    }
  }

  drawLine(location1, location2) {
    let pathLine = new window.google.maps.Polyline({
      path: [location1, location2],
      strokeColor: '#2d8e98',
      strokeOpacity: 0.8,
      strokeWeight: 2
    });
    pathLine.setMap(this.map);
    this.pathLines.push(pathLine);
  }

  removeLine() {
    this.pathLines.forEach(pathLine => {
      pathLine.setMap(null);
    })

    this.pathLines = []
  }

  removeAllMarkers() {
    this.markers.forEach(marker => {
      marker.setMap(null);
    })

    this.markers = []
  }

  addTruckMarkers(truck) {
    this.createMarker(this.buildIcon('icn-first-location.png', { w: 15, h: 15 }), truck.firstLocation);
    this.createMarker(this.buildIcon('icn-current-location.png'), truck.currentLocation);
    truck.path.forEach(location => this.createMarker(this.buildIcon('icn-path.png', { w: 10, h: 10 }), location));

    this.map.setCenter(truck.currentLocation);
  }

}
